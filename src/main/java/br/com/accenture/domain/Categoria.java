package br.com.accenture.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import com.google.cloud.firestore.annotation.DocumentId;

import lombok.Data;

/**
 * 
 * @author vanessa.araujo.silva@accenture.com
 *
 */

@Data
@Entity(name = "categoria")
public class Categoria implements Serializable {

	private static final long serialVersionUID = -1184877475934577810L;

	@DocumentId
	private String id;
	private String nome;
	private String descricao;

}
