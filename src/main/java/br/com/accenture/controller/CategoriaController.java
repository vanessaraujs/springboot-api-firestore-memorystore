package br.com.accenture.controller;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.cloud.firestore.Firestore;

import br.com.accenture.domain.Categoria;
import br.com.accenture.service.CategoriaService;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author vanessa.araujo.silva@accenture.com
 *
 */

@RestController
@RequestMapping(value = "/categoria")
public class CategoriaController {

	@Autowired
	Firestore firestore;

	@Autowired
	private CategoriaService categoriaService;

	@ApiOperation(value = "Lista as categorias")
	@GetMapping
	public List<?> findAll() throws InterruptedException, ExecutionException {
		return categoriaService.findAll();
	}

	@ApiOperation(value = "Busca uma categoria por ID")
	@GetMapping(value = "{id}")
	public ResponseEntity<Categoria> findById(@PathVariable String id) throws InterruptedException, ExecutionException {
		Categoria categoria = categoriaService.findById(id);
		return categoria != null ? ResponseEntity.ok(categoria) : ResponseEntity.notFound().build();
	}

	@ApiOperation(value = "Cadastra uma categoria")
	@PostMapping
	public ResponseEntity<Categoria> create(@Valid @RequestBody Categoria categoria)
			throws InterruptedException, ExecutionException {
		categoriaService.createCategoria(categoria);
		return new ResponseEntity<Categoria>(HttpStatus.CREATED);
	}
}
