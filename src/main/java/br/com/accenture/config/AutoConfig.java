package br.com.accenture.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Configuration;

import br.com.accenture.domain.Categoria;

/**
 * 
 * @author vanessa.araujo.silva@accenture.com
 *
 */
@Configuration
@EntityScan(basePackageClasses = { Categoria.class })
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ConditionalOnProperty(value = "spring.cloud.gcp.firestore.enabled", matchIfMissing = true)
public class AutoConfig {

}
