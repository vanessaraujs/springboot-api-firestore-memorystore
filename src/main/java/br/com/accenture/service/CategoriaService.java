package br.com.accenture.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;

import br.com.accenture.controller.CategoriaController;
import br.com.accenture.domain.Categoria;

/**
 * 
 * @author vanessa.araujo.silva@accenture.com
 *
 */

@Service
@CacheConfig(cacheNames = { "Categoria" })
public class CategoriaService {

	private static final String CATEGORIAS = "categorias";

	protected Logger logger = Logger.getLogger(CategoriaController.class.getName());

	@Autowired
	Firestore firestore;

	@Cacheable(cacheNames = "categorias", key = "#id")
	public Categoria findById(String id) throws InterruptedException, ExecutionException {

		ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = this.firestore.collection(CATEGORIAS).document(id)
				.get();
		Categoria payload = documentSnapshotApiFuture.get().toObject(Categoria.class);
		return payload;

	}

	@Cacheable(cacheNames = "listaCategorias")
	public List<?> findAll() throws InterruptedException, ExecutionException {
		List<Map<String, Object>> payload = new ArrayList<>();

		ApiFuture<QuerySnapshot> results = this.firestore.collection(CATEGORIAS).get();
		results.get().getDocuments().stream().forEach(action -> {
			payload.add(action.getData());
		});

		return payload;
	}

	public Categoria createCategoria(Categoria categoria) throws InterruptedException, ExecutionException {
		@SuppressWarnings("unused")
		WriteResult writeResult = this.firestore.collection(CATEGORIAS)
				.document(categoria.getId() != null ? categoria.getId() : UUID.randomUUID().toString()).set(categoria)
				.get();

		return categoria;
	}
}
